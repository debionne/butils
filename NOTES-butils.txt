
Notes about butils
==================

Butils is a tool intended to ....


Terminology:
------------

Component: Each of the folders to be treated by butils.
  - Components are declared in a butils.cfg and may contain their own butils.cfg file that is treated recursively by butils.
  - butils commands are always run on or refer to a given initial component that is treated as the root component.
  - With the exception of the root component, all the others are treated as subcomponents with a parent component.
  - All subcomponents require a default relative location <path> inside their parent component.
  - Subcomponents may optionally have a <name> used for identification purposes.
  - When needed for the build process, the subcomponent definition can declare the name of a variable <pathvar> to be used to locate the component.
  - All subcomponents have to be declared as either local or external.

Local component: a subfolder of the parent component in the same SVN repository.
  - Local components are checked out along their parents from the repository.
  - The relative location of a local component is fixed as defined by the default path that must match the structure of the repository.

External component: a subcomponent checked out from any SVN repository.
  - External components are always checked out separately from their parents, although they may come from the same repository if needed.
  - If an external component has already been declared by any of the ancestors of its parent component, the location of the previous declaration is used. Otherwise the component is checked out at the location specified by the default <path> in the current declaration.
  - An external component is defined by:
    . an SVN repository name <repo>. If <repo> is not specified, the component <name> is used instead.
    . the relative path <rfolder> of the component in the repository.
    . a mandatory field <verinfo> that specifies either the repository <branch> and <revision>, or a given <version> in the "releases" branch.
  - The following rules apply:
    . If <repo> does not include the full url of the repository, the prefix  https://deg-svn.esrf.fr/svn/ is assumed.
    . If <rfolder> is not specified, the component is assumed to be the root folder of the selected repository branch.
    . The <verinfo> field must follow one of the two the syntaxes:
        "[<branch>,] rev:<revision>"   where
            . if <branch> is not specified, the branch is assumed to be 'trunk'
            . <revision> is a SVN revision number that can be optionally replaced by the 'HEAD' string
            . the resulting url is obtained as:  -r <revision> <repo>/<branch>/<rfolder>
        "<version>"   where
            . the branch is assumed to be 'tags' and the revision HEAD
            . the resulting url is obtained as:  <repo>/tags/<version>/<rfolder>

Target: a component, local or external, that is flagged to be processed by butils during a build operation
  - The targets are specified in the [root] section of a butils.cfg file by a comma separated list in the <targets> field.
  - All the target components must be named (to be included in <targets>)

Checkout process:
  - The checkout process starts from a root component and propagates recursively to all the external components
  - The process to check out an external component, including the root component, is as follows:
      1. The component is checked out from the repository into its destination directory.
      2. If it contains a build.cfg file, the file is processed by identifying all its components
      3. Those external components that have not been previously checked out during the recursive process, are checked out in the order of declaration in the file.
      4. A _butils.mk file is created in the current component folder with all the <pathvar> location variables declared in the file initialised with the actual final location of the corresponding components.


Butils processing:
  - The project processing in butils happens in two phases: a mandatory 'checking' phase, followed by and optional 'building' phase.
  - During the 'checking' phase, the project tree is explored and if any external component is found missing, it is checked out from the repository.
  - Two additional variants are possible during the checking phase:  'update' and 'restore'
      - in 'update' .... update the svn revision to the 'nominal'
      - in 'restore' .... the project tree is processed by using the alternative config file butils.cfg.bak
  - During the building phase, only the declared targets are explored. The root component is always treated as a target.
  - The process to build a target, including the root component, is as follows:
     1. If the target includes a file 'Makefile', then 'make' is run in the target directory and the processing stops.
     2. If no Makefile but the target includes a butils.cfg file, then all the targets declared in the file are sequentially built in the order of declaration.
     3. Otherwise nothing happens.
  - As soon as an error happens, the build process is interrupted
  - Various 'rules' are possible to run make :  "build", "buidall", "cleanall", "release",


Root component:
---------------
[root]              //
defrev = <defrev>   // (optional) default revision <defrev> as "rev:#" or "rev:HEAD"
targets = <<list>>  // (optional) list of comma separated names of components to be
                    //    treated as targets during a build process

External component:
------------------
[external <name>]   // mandatory name for reporting and identification. default value for <repo>
path = <path>       // (mandatory) relative destination path to checkout the dependency
pathvar = <pathvar> // (optional) environment variable used to pass the component location to make
repo = <repo_url>   // (optional) default: https://deg-svn.esrf.fr/svn/<name>
rfolder = <relpath> // (optional) relative path after <repo_url>/<branch>/ ; default: ""
verinfo = <verinfo> // (mandatory) <verinfo> can be either "<branch>, <rev>", "<rev>" or "X.Y.Z"
                    //   where <rev> is "rev:#", "rev:HEAD" or "HEAD" and default branch is "trunk"
                    //   if "X.Y.Z" specified, url is <repo_url>/releases/<relpath>/X.Y.Z

Local dependencies:
-------------------
[local <name>]      // mandatory name for reporting and identification
path = <path>       // (mandatory) relative location path of the local dependency
pathvar = <pathvar> // (optional) environment variable used to pass the current location



==================================================================================

Other possible features/changes:
--------------------------------
- if the checkout branch is in releases, issue and error or propose to make a branch, but do not checkout directly
- butils should include options to clean working copies as well as to update/revert/check the repository revision of the current working copy. (full build = clean + build)
Build process:
- if "full" build, a clean operation is completed  before starting the build process
- produce a build.tmp with a the actual paths/branches/revisions used.
Release Conditions:
- release can be done only from the root directory of a repository (check this!!!)
- version:last not used in any place
- dependencies match the right url (repo/branch/path) and revision
- no changes in current working copy
- the release number is ok (no conflicts)
- a full build (clean+build) has been successfully completed during the release operation
- all the build products (result) are available (including butils products)
- more...
Release results:
   . replace build.cfg with build.tmp
   . a branch /releases/X.Y.Z is created with the products included

Issues:
-------
- What about targets that are not modified between releases? they will be rebuilt!!!
   . consider the possibility of releasing individual components

