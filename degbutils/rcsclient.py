# -*- coding: utf-8 -*-
from __future__ import print_function

import urllib.parse as urlparse
import subprocess
import re
import tempfile
import shutil

from . import cmdutils
from .import butils_defs

from .butils_defs import (BCFG_FNAME, MK_FNAME,
                         TRUNK, BRANCHES, TAGS,
                         HEAD,
                         LOCCOMP, EXTCOMP)


class SVNClient(object):
    """
    RCSClient that handles the specifics of SVN repositories.
    """
    def __init__(self):
        super(SVNClient, self).__init__()

    @staticmethod
    def checkout(url, revision, target_path):
        #if revision == BASE:
        print("Checking out {} at {}".format(url, target_path))
        #cmdutils.create_dirs(target_path)
        cmd = ["svn", "co", url, target_path]
        res = subprocess.call(cmd)

        # The call to svn returned a non zero exit code, something went wrong !
        if res:
            msg = 'Checkout failed; the url or revision given does not exist. '
            msg += 'Verify that the url points to a valid location and that '
            msg += 'revision exists'

            raise AssertionError(msg)

        return res

    @staticmethod
    def _item_info_tmp(path_url, items):
        item_name = {   'Working Copy Root Path' : 'wc-root',
                        'URL' : 'url',
                        'Relative URL' : 'relative-url',
                        'Repository Root' : 'repos-root-url',
                        'Repository UUID' : 'repos-uuid',
                        'Revision' : 'revision',
                        'Node Kind' : 'kind',
                        'Last Changed Author' : 'last-changed-author',
                        'Last Changed Rev' : 'last-changed-revision' }
        infolines = cmdutils.call(['svn', 'info', path_url]).split('\n')
        info_dict = {item_name[k]:v for l in infolines if ':' in l for k,v in [l.split(': ', 1)] if k in item_name}

        if type(items) is str:
            return info_dict[items]
        else:
            return [info_dict[item] for item in items]

    @staticmethod
    def item_info(path_url, items):
        return SVNClient._item_info_tmp(path_url, items)
        # The following code only works for SVN >= 1.9
        if type(items) is str:
            return cmdutils.call(['svn', 'info', '--show-item', items, path_url]).strip()
        else:
            return [SVNClient.item_info(path_url, item) for item in items]

    @staticmethod
    def _ancestor_path(root_url, path):
        url = root_url + path
        cmd = 'svn log -v --stop-on-copy {} | grep from | tail -n 1'.format(url)
        ans = cmdutils.call(['bash', '-c', cmd]).strip().split()
        #print(ans)
        if not ans:
            raise AssertionError("svn command failed")
        branch = ans[1]
        if not path.startswith(branch):
            raise AssertionError("No ancestors found")

        path = path[len(branch):].split('@')[0]
        ancestor = ans[3].replace(':', path+'@', 1)
        return ancestor


    @staticmethod
    def find_trunk_ancestor(path_url):
        url, repo_root = SVNClient.item_info(path_url, ['url', 'repos-root-url'])
        path = url[len(repo_root):]

        try:
            while not path.startswith(TRUNK):
                path = SVNClient._ancestor_path(repo_root, path)
            return path.split('@')[0]
        except:
            return None


    @staticmethod
    def local_version(path):
        url, revnum, root = SVNClient.item_info(path, ['url', 'revision','repos-root-url'])
        branch = url[len(root):]

        svnversion = cmdutils.call(['svnversion'], cwd=path).strip()
        status = (svnversion == revnum)

        return  'rev:' + revnum, branch, status

    @staticmethod
    def remote_version(path):
        svn_info = cmdutils.call(['svn', 'info'], cwd=path).strip()
        branch = TRUNK
        revnum = ''

        match = re.search(r"URL:\s(.*)", svn_info)

        if match:
            url = match.group(1).strip()
            svn_info = cmdutils.call(['svn', 'info', url], cwd=path).strip()
            match = re.search(r"Revision:\s(.*)", svn_info)

            if match:
                revnum = match.group(1).strip()
            else:
                revnum = '?'

            if butils_defs.BRANCHES in url:
                branch = butils_defs.BRANCHES
                branch += url.split(butils_defs.BRANCHES)[1]
            elif butils_defs.TAGS in url:
                branch = butils_defs.TAGS
                branch += url.split(butils_defs.TAGS)[1]

        return 'rev:' + revnum, branch

    @staticmethod
    def local_status(path):
        msg = ''
        msg += 'Checking status of working copy ' \
              + path + '...\n'

        svn_version = cmdutils.call(['svnversion'], cwd=path).strip()
        svn_status = cmdutils.call(['svn', 'st'], cwd=path).strip()

        msg += '---\nStatus:\n' + svn_status +"\n---\nRev: " + svn_version + '\n'
        if svn_version.find('M'):
            msg += "---\nWarning: Working copy has local modifications.\n"
        return msg

    @staticmethod
    def get_svninfo_prop(path, prop):
        svn_info = cmdutils.call(['svn', 'info'], cwd=path).strip()
        match = re.search(r"%s:\s(.*)" % prop, svn_info)
        prop_value = None

        if match:
            prop_value= match.group(1).strip()

        return prop_value

    @staticmethod
    def create_release(path, vernum):
        repo_root = SVNClient.get_svninfo_prop(path, 'Repository Root')
        repo_url = SVNClient.get_svninfo_prop(path, 'URL')
        relurl = SVNClient.get_svninfo_prop(path, 'Relative URL')

        if '-' in vernum:
            if re.search(r'^\d\.\d\.\d-?[a-zA-Z0-9_]+$', vernum) == None:
                raise AssertionError('The version number %s is invalid ' % vernum)
        else:
            if re.search(r'^\d\.\d\.\d$', vernum) == None:
                raise AssertionError('The version number %s is invalid ' % vernum)

        if not repo_root:
            raise AssertionError('Could not get repo root of %s' % path)

        release_url = urlparse.urljoin(repo_root + butils_defs.TAGS)
        release_url = urlparse.urljoin(release_url + '/', vernum)

        try:
            cmd = ['svn', 'info', release_url]
            subprocess.check_call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as ex:
            pass
        else:
            msg = "\nCould not create release, release with version %s "
            msg += "already exists\n"
            msg = msg % vernum
            print(msg)
            return -1

        if relurl != "^" + TRUNK:
            msg = "\nCan't release subprojects or parts of a project, "
            msg += "retry with project root\n"
            print(msg)
            return -1

        ans = cmdutils.query_yes_no('Create release %s' % vernum)

        if ans:
            try:
                cmd = ['svn', 'copy', '-m "New release: %s"' % vernum, repo_url,
                       release_url]
                subprocess.check_call(cmd)

            except subprocess.CalledProcessError as ex:
                msg = "Could not create release, verify that a release with version "
                msg += "%s does not already exist"
                msg = msg % vernum
                print(msg)
                return -1
            else:
                print("Created release branch %s" % release_url)

            try:
                dirpath = tempfile.mkdtemp()

                cmd = ['svn', 'co', '--depth', 'files', release_url, dirpath]
                subprocess.check_call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

                cmd = ['cp', path + '/._'+BCFG_FNAME, dirpath + '/'+BCFG_FNAME]
                subprocess.check_call(cmd)

                cmd = ['cp', path + '/bof.txt', dirpath + '/bof.txt']
                subprocess.check_call(cmd)

                cmd = ['svn', 'add', dirpath + '/bof.txt', dirpath + '/'+BCFG_FNAME]
                subprocess.call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

                cmd = ['svn', 'ci', '-m "Updated ' + BCFG_FNAME + '"' ,
                       dirpath + '/'+ BCFG_FNAME,  dirpath + '/bof.txt']
                subprocess.check_call(cmd,  stdout=subprocess.PIPE, stderr=subprocess.PIPE)

                shutil.rmtree(dirpath)
            except subprocess.CalledProcessError as ex:
                print("Could not update bof.txt and/or " + BCFG_FNAME)
                return -1
            else:
                print("Updated " + BCFG_FNAME + " and bof.txt")
                print('\n\nRelease: %s successfully created !\n\n' % vernum)
