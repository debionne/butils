# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import os
import getpass
import time
import _version

from operator import itemgetter

from columnize import columnize

from butils_defs import BCFG_FNAME


class Builder(object):
    @staticmethod
    def build(project_path, extra_args={}):
        """
        Starts the build process

        :param str project_path: The full path to the directory that contains the
                                 butils.cfg
        :param extra_args dict: Arguments that will override the arguments
                                that are globally defined in the butils.cfg,
                                for instance arguments passed on the command
                                line.

        """
        os.environ["BUTILS_ROOT_DIR"] = os.path.abspath(project_path)
        os.environ["BUTILS_VER"] = _version.VERSION
        print("Butils root dir :" + os.environ["BUTILS_ROOT_DIR"])

        # Build the project
        Builder.build_project(project_path)

        # Since the project makefile deals with the entire build
        # and doesn't provide versioning info, we must get the project status
        # after the build process
        print("Generating project status:")
        bitem_dict = {}
        args = (project_path, None, bitem_dict)
        bitem_dict = Builder.recursive_project_status(*args)
        print()
        return bitem_dict

    @staticmethod
    def build_project(bfile_dir):
        """
        Build the targets (recursively).
        """
        # Get rid of any excess redirections in the path variable
        bfile_dir = os.path.normpath(bfile_dir)
        bfile_path = os.path.join(bfile_dir, BCFG_FNAME)

        if not project_root:
            project_root = bfile_dir

        # Otherwise get to business
        #comp_list, target_list, root_rev = Parser.parse_butils_cfg(bfile_path)

        if path in build_dict:
            build_path = os.path.join(bfile_dir, build_dict['path'])
        else:
            build_path = bfile_dir

        Builder.print_header("Building project")
        Builder.make(build_path, build_args, target)
        #Builder.update_target_version_status(target, bfile_dir)
        #Builder.finalize(bfile_dir, conf['targets'])


    @staticmethod
    def recursive_project_status(bfile_dir, project_root=None,
                      bitem_info_dict={}):
        """
        Recursive parsing of the project to get status information.

        :param bfile_dir:
        :param project_root:
        :param bitem_info_dict:
        :return:
        """
        # Get rid of any excess redirections in the path variable
        bfile_dir = os.path.normpath(bfile_dir)
        bfile_path = os.path.join(bfile_dir, BCFG_FNAME)

        sys.stdout.write('.')
        sys.stdout.flush()



    @staticmethod
    def print_header(msg):
        print(79*'-')
        print('    {} '.format(msg))
        print(79*'-')

    @staticmethod
    def update_cmp_version_status(cmp, cmp_path):
        """
        Update a component with the current version information.

        :param BComponent cmp: component to update
        :param str cmp_path: The root path of the component
        """

        # If the last keyword was used, get the latest version
        # from the rcs (remote)
        if 'last' in cmp.version:
            rver = SVNClient.remote_version(cmp_path)
            cmp.version = rver[0]

        (rcs_version, rbranch, _) = SVNClient.local_version(cmp_path)
        cmp.rcs_version = rcs_version
        cmp.rbranch = rbranch

    @staticmethod
    def update_target_version_status(target, path):
        """
        Updates the target with the current version information and checks if
        the versions of the dependencies specified in the butils.cfg corresponds
        to the ones on the file system (reported by the rcs)

        :param BComponent target: Target to update
        :param str path: The root path of the target
        """
        # Whats actually on the file system ?, Get the version branch
        # and version status from the rcs
        (rcs_version, rbranch, status) = SVNClient.local_version(path)

        target.rcs_version = rcs_version
        target.rbranch = rbranch
        target.rcs_status = status

    @staticmethod
    def bitem_info_list(bitem, path, dep_root, idx):
        """
        Creates bitem status list for a BComponent.
        """
        main_target = False if dep_root in path else True
        return [bitem, main_target, idx]

    @staticmethod
    def format_bitem_info_dict(bitem_info_dict):
        """
        Creates a pretty str representation of the build item info dict
        """
        _date = time.strftime('%Y-%m-%d', time.localtime())
        _time = time.strftime('%H:%M:%S', time.localtime())

        header = "Built %s at %s " % (_date, _time)
        header += "on %s by %s \n\n" % (os.uname()[1], getpass.getuser())

        items = []

        for bs in bitem_info_dict.values():
            bitem, main_target = bs[0], bs[1]

            if main_target:
                display_name = bitem._full_path
            else:
                display_name = bitem._full_path

            str_list = [bs[-1], display_name]
            str_list.append(bitem.rbranch)

            # Is the source modified add a *
            if bitem.rcs_status:
                str_list.append(bitem.rcs_version)
            else:
                str_list.append(bitem.rcs_version + '(*)')

            # Is this item a main target, add target
            if main_target:
                str_list.append('target ')
            else:
                str_list.append('component')

            # Add a question mark (!) if the version in the rcs is different
            # from the one requested.
            if bitem.cmp_list:
                cmp_str = '('

                for cmp in bitem.cmp_list:
                    if cmp.cfg_rcs_status:
                        cmp_str += '%s %s, ' % (cmp.name, cmp.version)
                    else:
                        cmp_str += '%s %s %s, ' % (cmp.name, cmp.version, '(!)')

                # Remove last comma and space
                cmp_str = cmp_str[0:-2]
                str_list.append(cmp_str + ')')
            else:
                str_list.append('')

            items.append(str_list)

            bargs = bitem.args.iteritems()
            # print(bargs)
            bargs_str = ', '.join(["%s=%s" % (n, v) for (n, v) in bargs])
            str_list.append(bargs_str)

        # Sort in build order
        items = sorted(items, key=itemgetter(0))

        # Removing numbers column
        items = [item[1:] for item in items]
        contents = header + columnize(items) + '\n'

        return contents

    @staticmethod
    def assemble_build_spec(bitem_info_dict):
        item_list = []
        for bs in bitem_info_dict.values():
            item_list.append((bs[-1], bs[0], bs[1]))

        item_list = sorted(item_list, key=itemgetter(0))
        target_list, cmp_list = [], []

        for item in item_list:
            bitem, main_target = item[1], item[2]

            if main_target:
                target_list.append(bitem)
            else:
                cmp_list.insert(0, bitem)

        spec_content = ""

        for bitem in target_list:
            spec_content += bitem.to_cfg_entry(True)

        for bitem in cmp_list:
            spec_content += bitem.to_cfg_entry(False)

        return spec_content

    @staticmethod
    def print_bof(path):
        """
        Prints the bof file in <path> to stdout.

        :param str path: A path
        """
        bof_path = os.path.join(path, 'bof.txt')

        with open(bof_path, 'r') as f:
            print(''.join(f.readlines()))

    @staticmethod
    def write_bof(path, status_dict):
        """
        Creates a Build Output File (BOF) and writes it to <path>
        """
        bof_path = os.path.join(path, 'bof.txt')

        contents = Builder.format_bitem_info_dict(status_dict)

        dirty = False

        for bitem_status in status_dict.values():
            if not bitem_status[0].rcs_status:
                dirty = True
                break

            for dep in bitem_status[0].cmp_list:
                if not dep.cfg_rcs_status:
                    dirty = True
                    break

        with open(bof_path, 'w+') as f:
            f.write(contents)

        print(contents)

        if dirty:
            msg = "Warning the source tree is dirty, see lines marked with "
            msg += "((*) or (!)) ! \n\n"
            msg += "* The source have been modified or just partially updated\n"
            msg += "! The version given in the butils.cfg file does not match "
            msg += "the one on the file system  \n\n"
            msg += "Please correct these issues before continuing \n"
        else:
            Builder.write_spec(path, status_dict)
            msg = 'OK to release ! \n\n'
        print(msg)


    @staticmethod
    def write_spec(path, bitem_info_dict):
        with open(os.path.join(path, '._'+BCFG_FNAME), 'w+') as f:
            f.write(Builder.assemble_build_spec(bitem_info_dict))

    @staticmethod
    def verify_for_release(path):
        """
        Checks if source tree at <path> is releasable.
        """
        (rcs_version, branch, status) = SVNClient.local_version(path)

        # The sources have been modified
        if not status:
            msg = '\n\nThere are local modifications to the source, \n'
            msg += 'Commit and build again before creating the release. \n\n'
            print(msg)
            return False

        # Is there a bof and (1) is the bof clean (2) does it correspond
        # to the revision on fs.
        if os.path.isfile(os.path.join(path, 'bof.txt')):
            lines = []
            bof_is_dirty = False

            with open(os.path.join(path, 'bof.txt'), 'r') as f:
                for line in f:
                    line = line.strip()

                    if line:
                        if ('*'in line) or ('!' in line):
                            bof_is_dirty = True

                        line = [v for v in line.split(' ') if v !='']
                        lines.append(line)

            # bof is not clean; there are version differences between
            # required dependencies and actually built ones, or/and there
            # were local changes when the build was performed
            if bof_is_dirty:
                Builder.print_bof(path)
                msg = "Warning the source tree was dirty when the build "
                msg += "was done,\nsee lines marked with ((*) or (!)) ! \n\n"
                msg += "* The source have been modified or just partially "
                msg += "updated\n! The version given in the butils.cfg file "
                msg += "does not match \n  the one on the file system \n\n"
                msg += "Please correct these issues before continuing \n\n"

                print(msg)
                return False

            # Does the build correspond to the one in the bof ?
            t = lines[-1]
            (version, branch, status) = SVNClient.local_version(path)

            if (t[1] == branch) and (t[2] == version) and (t[3] == 'target'):
                Builder.print_bof(path)
                print('OK to release ! \n\n')
                return  True
            else:
                msg = '\n\nThe bof.txt file is outdated it corresponds to a '
                msg += 'older revision. Please re-build to make it up to '
                msg += 'date \n\n'
                print(msg)
                return False
        else:
            msg = '\nThe previous build is not valid for release '
            msg += '(no bof.txt file has been generated in %s)\n' % path
            msg += 'In case of release make sure that you are creating a release'
            msg += ' for the entire project and not a subproject.\n'
            print(msg)
            return False

