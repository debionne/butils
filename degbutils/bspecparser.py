# -*- coding: utf-8 -*-
"""
Module for parsing butils definitions (butils.cfg) files. The file format
is based on the .ini file format. The major difference from the original
.ini format is that a name property can be given in the section definition,
like:

::

  [section <name>]

This will add a keyword called name with the value <name> to the section. The
parsing is based on the Python module ConfigParser.

The parsing is performed by the parse_butils_cfg method. The parsing is done in two
qpasses; each section is first validated and parsed as they are in a first pass,
and then all references, if any, are resolved and validated.
"""
from __future__ import print_function

import sys
import os

from .butils_defs import *
from . import cmdutils
from . import rcsclient

DEBUG = False


LOCDEP_DEPENDENCY_TEMPLATE =\
"""
[local {name}]
path = {path}
pathvar = {pathvar}
"""

EXTDEP_ENTRY_TEMPLATE =\
"""
[external {name}]
path = {path}
pathvar = {pathvar}
repo = {repo}
rbranch = {rbranch}
rfolder = {rfolder}
verinfo = {verinfo_str}
"""

def write_butils_mk_file(path, extpaths, pathvars):
    header =  ("# Do not edit manually!\n"
               "#\n"
               "# File {} automatically generated by butils. \n"
               "# Component location and Makefile variables relative to path:\n"
               "#    {}\n").format(MK_FNAME, path)

    mk_path = os.path.join(path, MK_FNAME)
    with open(mk_path, 'w') as f:
        f.write(header)
        if extpaths:
            f.write("#\n# External components:\n")
            for name in extpaths:
                comp = extpaths[name]
                f.write("{}{} rev:{} @ {}\n".format(MK_EXTCOMP_PRFX, name, comp['rev'], comp['path']))
        if pathvars:
            f.write("#\n# Makefile path variables:\n")
            for var in pathvars:
                f.write("{} = {}\n".format(var, pathvars[var]))


def read_extcomps_mk_file(fullpath):
    mk_path = os.path.join(fullpath, MK_FNAME)
    extpaths = {}
    try:
        with open(mk_path, 'r') as f:
            alllines = f.readlines()
    except:
        alllines = []
        #raise ButilsError('missing or not accessible {} file'.format(mk_path))

    for line in alllines:
        try:
            if line.startswith(MK_EXTCOMP_PRFX):
                parts = line[len(MK_EXTCOMP_PRFX):].split(' @ ')
                path = os.path.join(fullpath, parts[1].strip())
                parts = parts[0].split(' rev:')
                name = parts[0]
                rev = parts[1]
                extpaths[name] = {'path':path, 'rev':rev}
        except:
            raise ButilsError('badly formed line in {}: {}'.format(mk_path, line))
    #print(extpaths)
    return extpaths


def check_token_format(token, errmsg, isrootpath=False):
    for c in token:
        if c.isspace():
            raise ButilsError(errmsg)
    if isrootpath and token[0] != '/':
        raise ButilsError(errmsg + ', should it be /{} ?'.format(token))


class BComponent(object):
    """
    Object representing a butils component (root, local or external)

    :param dict kwargs: Dictionary containing the keys; name, repo, branch,
                        path, verinfo, method, args. All of type str.
    """

    def __init__(self, type, path, kwargs={}, name=None, parent=None, url=None):
        # common to targets and dependencies
        self.type = type
        self.name = name
        self.parent = parent
        self.target_names = []
        self.subcomp = []
        self.path = path
        self.fullpath = os.path.join(parent.fullpath, path) if parent else path

        self.pathvar = kwargs.get(KW.pathvar, None)

        if os.path.isdir(self.fullpath):
            try:
                self.rcs_url, self.rcs_rev, root = rcsclient.SVNClient.item_info(self.fullpath, ['url', 'revision','repos-root-url'])
            except:
                errmsg = 'Folder {} is not under SVN revision control'.format(self.fullpath)
                raise ButilsError(errmsg)

        elif os.path.exists(self.fullpath):
            raise ButilsError("Local path {} is not a folder".format(self.fullpath))
        else:
            self.rcs_url = None
            self.rcs_rev = None

        if type == LOCCOMP:
            if not self.rcs_url:
                raise ButilsError("Folder {} for local component is missing".format(self.fullpath))
            if self.rcs_url != parent.rcs_url + '/' + path:
                raise ButilsError("Local folder {} is not in parent's repository".format(self.fullpath))
            self.rcs_repo = parent.rcs_repo
            self.rcs_rfolder = parent.rcs_rfolder + '/' + path
            self.url = self.rcs_url
            self.rev = self.parent.rev
            # TODO: check consistency of self.rcs_rev with parent.rev

        elif type == ROOTCOMP:
            self.url = url if url else self.rcs_url
            self.rev = HEAD
            try:
                self.rcs_repo = rcsclient.SVNClient.item_info(self.url, 'repos-root-url')
            except:
                raise ButilsError('Bad repository url: {}'.format(self.url))
            self.rcs_rfolder = None

        elif type == EXTCOMP:
            # optional fields
            if KW.repo in kwargs:
                repo = self.repo = kwargs[KW.repo].strip('/')
            else:
                self.repo = None
                repo = name

            if '://' not in repo:
                repo = REPO_BASEURL + repo

            self.rcs_repo = repo

            self.rfolder = kwargs[KW.rfolder].rstrip('/') if KW.rfolder in kwargs else None
            self.rcs_rfolder = self.rfolder if self.rfolder else TRUNK
            if not self.rcs_rfolder.startswith(TRUNK):
                raise ButilsError("{} field '{}' does not start by {}".format(KW.rfolder, self.rcs_rfolder, TRUNK))

            # mandatory field
            self.verinfo = kwargs[KW.verinfo]

            # Check if the verinfo contains a version id or a
            # branch and revision or version with the format [<branch-path>,] rev:<n>
            version = self.verinfo.split(',')
            if len(version) == 1:
                branch = None
                version = version[0].strip()
            elif len(version) == 2:
                branch = version[0].strip().rstrip('/')
                check_token_format(branch, "bad branch in verinfo description", isrootpath=True)
                version = version[1].strip()
            else:
                raise ButilsError("bad verinfo description")
            check_token_format(version, "bad revision or version in verinfo description")

            # update self.rev and self.url with the info from 'verinfo' field
            self.rev, version = Parser.parse_revision(version)

            if version:
                if branch:
                    if not branch.startswith(TAGS):
                        raise ButilsError("bad dependency branch: {}".format(branch))
                else:
                    branch = TAGS

                self.url = '{}{}/{}'.format(repo, branch, version)

            else:
                if not branch:
                    branch = self.rcs_rfolder
                self.url = repo + branch


        """
        # Version on the file system (given by the rcs)
        self.rcs_version = kwargs.get('rcs_version', None)

        # Mismatches between rcs_version of a dependency and version given in
        # the butils.cfg. True if matching otherwise False
        self.cfg_rcs_status = True

        # Are the sources on fs un-modified: True else False
        self.rcs_status = True
        """

    def check_revision(self, check_rule=None):
        """
        Checkout a project (full source) tree of <url> to <bfile_dir>

        :param url:         URL to the project repo
        :param bfile_dir: target path for the project to be checked out to
        :param repo_type:   type of repo (SVN only for now)
        :return:
        """
        #if os.path.exists(self.fullpath):
        #if os.path.isdir(self.fullpath) and os.listdir(self.fullpath):

        # check what to do, and if needed, checkout....
        if not self.rcs_url or check_rule is RULE.update:
            rcsclient.SVNClient.checkout(self.url, self.rev, self.fullpath)
            self.rcs_url, self.rcs_rev = rcsclient.SVNClient.item_info(self.fullpath, ['url', 'revision'])

        if not self.rcs_rfolder:
            self.rcs_rfolder = rcsclient.SVNClient.find_trunk_ancestor(self.rcs_url)
        # now check that url and revision are consistent


    def is_already_present(self, comp_list, old_externals, ignore_rev=False):
        for comp in comp_list:
            #if self.url == comp.rcs_url and self.rev == comp.rev:
            #print((self.rcs_repo,comp.rcs_repo), "----", (self.rcs_rfolder,comp.rcs_rfolder))
            if self.rcs_repo == comp.rcs_repo and self.rcs_rfolder == comp.rcs_rfolder:
                if ignore_rev or self.rev == comp.rev:
                    return comp.fullpath

        if self.name in old_externals:
            rev = old_externals[self.name]['rev']
            fullpath = old_externals[self.name]['path']
            if '..' in fullpath:
                print("TODO:", self.name, ">>>> ", fullpath, self.parent.fullpath)
                if os.path.isdir(fullpath):
                    try:
                        rcs_url, rcs_rev, root = rcsclient.SVNClient.item_info(fullpath, ['url', 'revision','repos-root-url'])
                        if True:  # check that components are 'the same'
                            return fullpath
                    except:
                        pass

        return None


    def check_subcomponents(self, check_rule=None, parent_items=[]):
        """
        Checkout a project (full source) tree of <url> to <bfile_dir>

        :param url:         URL to the project repo
        :param bfile_dir: target path for the project to be checked out to
        :param repo_type:   type of repo (SVN only for now)
        :return:
        """

        # check parent component first and flag if it is the good one
        #if self.url != self.rcs_url or self.rcs_rev != self.rcs_rev:
        #    print("mismatch ... ({})".format(self.fullpath));

        # now extract subcomponents by processing butils.cfg or butils.cfg.bak
        restore = check_rule is RULE.restore
        self.defrev, self.subcomp, self.target_names, old_externals = Parser.parse_butils_cfg(self, restore)

        if not self.subcomp:
            return

        if DEBUG:
            print(        "Found components:")
            for comp in self.subcomp:
                pathvar_str = "[{}]".format(comp.pathvar) if comp.pathvar else ""
                print(     "  - {} at {} {}".format(comp.name, comp.fullpath, pathvar_str))
                if comp.type == EXTCOMP:
                    revstr = "rev:{}".format(comp.rev) if (comp.rev != HEAD) else ""
                    print("      svn:", revstr, comp.url)
            print()

        chkout_items = parent_items[:]   # make a copy to not modify original

        newcomp = []
        extpaths = {}
        pathvars = {}
        for comp in self.subcomp:
            fullpath = comp.is_already_present(chkout_items, old_externals)

            if fullpath:  # if comp was in the chkout_items list
                comp.fullpath = fullpath
            else:
                # check out the new component
                comp.check_revision(check_rule)

                #augment newcomp and chkout_items with the components not in the list
                newcomp.append(comp)
                chkout_items.append(comp)

            rel_path = os.path.relpath(comp.fullpath, self.fullpath)
            if comp.type == EXTCOMP:
                extpaths[comp.name] = {'path':rel_path, 'rev':comp.rcs_rev}

            if comp.pathvar:
                pathvars[comp.pathvar] = rel_path

        write_butils_mk_file(self.fullpath, extpaths, pathvars)

        for comp in newcomp:
            comp.check_subcomponents(check_rule, chkout_items)


    def find_target(self, target_chain):
        first_comp = target_chain[0]
        next_comps = target_chain[1:]
        compnames = [comp.name for comp in self.subcomp]
        if not first_comp in compnames:
            raise ValueError('no component {} in {}'.format(first_comp, self.name))
        comp = self.subcomp[compnames.index(first_comp)]
        if not next_comps:
            return comp
        else:
            return comp.find_target(next_comps)

    def check_targets(self, check_rule=None):
        self.targets = []
        for target_name in self.target_names:
            try:
                try:
                    target = self.find_target(target_name.split('.'))
                except ValueError as ex:
                    raise ValueError('bad target name "{}": {}'.format(target_name, ex.message))

                if target in self.targets:
                    raise ValueError('repeated target "{}"'.format(target_name))
                else:
                    self.targets.append(target)
            except ValueError as ex:
                print('Checking targets of {} @ {}:'.format(self.name, self.fullpath))
                raise ex

        for comp in self.subcomp:
            comp.check_targets(check_rule)


    def process(self, build_rule, vernum=None):
        """
        """

        if not build_rule: return

        name = self.name if self.name else ''

        print('Processing component {} @ {}'.format(name, self.fullpath))

        if os.path.isfile(os.path.join(self.fullpath, 'Makefile')):
            print('  Building {} via Makefile'.format(name))

            cmd = 'make {}'.format(build_rule)

            print('  Running "{}" in {}: \n\n'.format(cmd, self.fullpath))
            cmdutils.call(cmd, stdout=sys.stdout, cwd=self.fullpath, env=os.environ)

        elif self.targets:
            print('  Building {} via butils'.format(name))
            for target in self.targets:
                target.process(build_rule)

        else:
            print('  Cannot build {}: no Makefile, no targets'.format(name))


    def to_cfg_entry(self):

        cfg_entry = \
        """
        [{type} {name}]
        path = {path}
        """.format(type = self.type, name = self.name, path = self.path)

        if self.pathvar:
            cfg_entry += "pathvar = {}\n".format(self.pathvar)

        if self.type == EXTCOMP:
            if self.repo:
                cfg_entry += "repo = {}\n".format(self.repo)
            if self.rfolder:
                cfg_entry += "rfolder = {}\n".format(self.rfolder)

            cfg_entry += "verinfo = {}\n".format(self.verinfo)

        return cfg_entry


class Parser(object):
    def __init__(self):
        super(Parser, self).__init__()
        self._dep_rootpath = None
        self.global_args = {}

    @staticmethod
    def parse_revision(verinfo_str, no_version=False):
        revision = HEAD
        version_tag = None
        verinfo_str = verinfo_str.strip()

        if verinfo_str[0:4] == 'rev:':
            revision = verinfo_str[4:]
            if revision != HEAD and not revision.isdigit():
                raise AssertionError("bad revision number")

        elif verinfo_str != HEAD:
            if no_version:
                raise AssertionError("invalid revision number")
            version_tag = verinfo_str

        if no_version:
            if version_tag:
                raise AssertionError("invalid revision number")
            return revision
        else:
            return revision, version_tag

    @staticmethod
    def _valid_keywords(items, section_type, name):
        """
        Checks if the keywords in items exists in the given keyword_list.

        :param list items: list of pairs (keyword, value) to validate
        :param list keyword_list: list of valid keywords to validate against

        :raises ValueError: if any if the items are invalid
        :returns: if all items are valid, a dictionary with the values
        """
        if name:
            errmsg = 'section [{} {}]'.format(section_type, name)
        else:
            errmsg = 'section [{}]'.format(section_type)

        if section_type == LOCCOMP:
            kw_list_dict = LOCCOMP_KEYWORDS
        elif section_type == EXTCOMP:
            kw_list_dict = EXTCOMP_KEYWORDS
        elif section_type == ROOTCOMP:
            kw_list_dict = ROOTCOMP_KEYWORDS
        else:
            raise ValueError("bad {}".format(errmsg))

        kwargs = {}
        for item in items:
            key, value = item
            if key not in kw_list_dict['mandatory'] and key not in kw_list_dict['optional']:
                msg = 'unexpected keyword "{}" in {}'.format(key, errmsg)
                raise ValueError(msg)
            kwargs[key] = value

        for key in kw_list_dict['mandatory']:
            if key not in kwargs:
                msg = 'missing keyword "{}" in butils.cfg'.format(key)
                raise ValueError(msg)

        return kwargs


    @staticmethod
    def parse_section_entry(section_entry, items):
        """
        Handles the value of the name keyword passed in the section header.

        :param str section_name: The section name
        :returns: The tuple section type, name, kwargs
        """

        entry = section_entry.split()
        if len(entry) > 2 :
            raise ValueError('bad formed section [{}]'.format(section_entry))

        type = entry[0]
        try:    name = entry[1]
        except: name = None

        kwargs = Parser._valid_keywords(items, type, name)

        return type, name, kwargs


    @staticmethod
    def extcomponent_exists(extcomp, comp_list):
        """
        Checks if a dep or component is already present in a list of
        already checked out extdeps/locdeps

        :param BComponent extcomp: an external component
        :param list            comp_list: A list containing ButilsComponents
                              (           to search in.

        :returns: None if no match is found, PATH to extcomp if found.
        """
        for comp in comp_list:
            if extcomp.url == comp.rcs_url and extcomp.rev == comp.rev:
                return comp.path
        return None


    @staticmethod
    def parse_butils_cfg(maincomp, restore=None):
        """
        Parses the butils.cfg file at at given path

        :param str bfile_dir: the directory containing the cfg file
        :returns: A tuple containing the parsed items in the following
                  form: (components[], targets[], defrev)
        :rtype: dict
        """

        bcfg_filename = BCFG_BAK_FNAME if restore else BCFG_FNAME
        bfile_path = os.path.join(maincomp.fullpath, bcfg_filename)

        # If there is no config file at bfile_path, return None
        if not os.path.exists(bfile_path):
            return None, [], [], None

        try:
            try:    # first Python3
                from configparser import ConfigParser
            except: # then Python2
                from ConfigParser import ConfigParser

            parser = ConfigParser()
            # Setting optionsxform to str so that options are case sensitive
            parser.optionxform = str

            try:
                print("Processing config file:", bfile_path)
                parser.read(bfile_path)
            except Exception as ex:
                raise ValueError(ex)

            components = []
            targetlist = []
            defrev = None
            for section in parser.sections():
                comp_type, name, kwargs = Parser.parse_section_entry(section, parser.items(section))

                if comp_type == ROOTCOMP:
                    if name:
                        raise ValueError('[{}] section must be anonymous'.format(ROOTCOMP))
                    if targetlist or defrev:
                        raise ValueError('duplicated [{}] section'.format(ROOTCOMP))
                    if KW.targets in kwargs:
                        targetlist = kwargs[KW.targets].split(',')
                        targetlist = [t.strip() for t in targetlist]
                    if KW.defrev in kwargs:
                        defrev = Parser.parse_revision(kwargs[KW.defrev], no_version=True)
                    else:
                        defrev = HEAD

                else:  # local or external component section
                    if not name:
                        raise ValueError('missing component name in [{}] section'.format(comp_type))
                    path = kwargs[KW.path]
                    newcomp = BComponent(comp_type, path, kwargs, name=name, parent=maincomp)
                    for comp in components:
                        if newcomp.url == comp.url or newcomp.name == comp.name:
                            raise ValueError('repeated component: "{}"'.format(name))
                    components.append(newcomp)

        except ValueError as ex:
            print('Parse error in {}, {}'.format(bfile_path, ex.message))
            sys.exit(2)
        else:
            #return components, targetlist, defrev
            oldexternal = read_extcomps_mk_file(maincomp.fullpath)
            return defrev, components, targetlist, oldexternal
