# -*- coding: utf-8 -*-
import os
import sys
import inspect


class ComponentsDir(object):
    def __init__(self, path, fpath = None):
        self.import_paths = []

        if not fpath:
            calframe = inspect.getouterframes(inspect.currentframe(), 2)
            fpath = os.path.abspath(inspect.getfile(calframe[1][0]))

        rootpath = os.path.abspath(os.path.join(os.path.dirname(fpath), path))

        if os.path.isdir(rootpath):
            for dirname in os.listdir(rootpath):
                self.import_paths.append(os.path.join(rootpath, dirname))
        else:
            print('LocalImport: path %s does not exist !' % rootpath)

    def __enter__(self):
        for path in self.import_paths:
            print('LocalImport: importing from %s' % path)
            sys.path.insert(0, path)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        for path in self.import_paths:
            sys.path.remove(path)
