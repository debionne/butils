# -*- coding: utf-8 -*-


class ButilsError(Exception):
    pass

# Repo defaults and settings

REPO_BASEURL = 'https://deg-svn.esrf.fr/svn/'

class KW(object):
    MANDATORY = 'mandatory'
    OPTIONAL  = 'optional'
    path      = 'path'
    pathvar   = 'pathvar'
    verinfo   = 'verinfo'
    repo      = 'repo'
    rfolder   = 'rfolder'
    targets   = 'targets'
    defrev    = 'defrev'

    url       = 'url'
    vernum    = 'vernum'


# Label for repository branches
TRUNK    = '/trunk'
BRANCHES = '/branches'
TAGS     = '/tags'


# Labels for generic revisions
BASE      = 'BASE'
HEAD      = 'HEAD'

class RULE(object):
    update   = 'UPDATE'
    restore  = 'RESTORE'
    build    = 'build'
    buildall = 'buildall'
    release  = 'release'
    cleanall = 'cleanall'


PREDEF_REPOS = [{'url': REPO_BASEURL,
                 'tag_pattern': 'tag/{name}-{version}',
                 'type': 'svn',
                 'name': 'deg-svn',
                 'branch': TRUNK + '/'}]


# General defaults and settings for butils tool

BCFG_FNAME =     'butils.cfg'
BCFG_BAK_FNAME = 'butils.cfg.bak'
MK_FNAME   = '_butils.mk'

MK_EXTCOMP_PRFX   = '## '

LOCCOMP  = 'local'
EXTCOMP  = 'external'
ROOTCOMP = 'root'

# Allowed keywords for local components
LOCCOMP_KEYWORDS =  {KW.MANDATORY: [KW.path],
                     KW.OPTIONAL : [KW.pathvar] }

# Allowed keywords for external components
EXTCOMP_KEYWORDS =  {KW.MANDATORY: [KW.path, KW.verinfo],
                     KW.OPTIONAL : [KW.repo, KW.rfolder, KW.pathvar] }

# Allowed keywords for root components
ROOTCOMP_KEYWORDS = {KW.MANDATORY: [],
                     KW.OPTIONAL : [KW.targets, KW.defrev] }


