# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import os
from argparse import ArgumentParser, RawDescriptionHelpFormatter

from . import _version

from .bspecparser import BComponent
from .butils_defs import REPO_BASEURL, TRUNK, ROOTCOMP, RULE


def urlarg_parse(url, use_fpath=False):
    if 'http://' in url or 'https://' in url:
        return url, None

    if use_fpath:
        url = os.path.basename(url)

    if '/' in url:
        name = url.split('/')[-1]
        url = REPO_BASEURL + url
    else:
        name = url
        url = REPO_BASEURL + url + TRUNK

    return url, name


def incompatible_and_exit(options, msg=None):
    if not msg:
        msg = "Incompatible options: {}".format(options)
    print(msg)
    sys.exit(2)


def main():
    descr = ('D&E Group tool to automate the checkout, build and release of\n'
             'development projects.')
    parser = ArgumentParser(description=descr, epilog=' ',
                            formatter_class=RawDescriptionHelpFormatter,
                            conflict_handler='resolve')
    parser.add_argument('--version', action='version', version=_version.VERSION)
    parser.add_argument('path', nargs='?', metavar='<project-folder>',
                        help='folder to be processed. '
                             'Default is the current folder')

    chkgroup = parser.add_mutually_exclusive_group()
    chkgroup.add_argument('-c', '--checkout', metavar='<repo>',
                        help='checkout project from the SVN repository <repo>')
    chkgroup.add_argument('-u', '--update', action='store_true',
                        help='update the project from the SVN repository')

    bldgroup = parser.add_mutually_exclusive_group()
    bldgroup.add_argument('-x', '--cleanall', action='store_true',
                        help='clean the project with "cleanall" rule')
    bldgroup.add_argument('-b', '--build', action='store_true',
                        help='build the project with "build" rule')
    bldgroup.add_argument('-a', '--buildall', action='store_true',
                        help='build the project with "buildall" rule')
    bldgroup.add_argument('--release', metavar='<tag>',
                        help='buildall and release the project with tag <tag>')

    parser.add_argument('-v', '--verify', action='store_true',
                        help='during a release operation, only build and verify '
                             'the results. Do not commit any changes to the '
                             'repositories')

    args = parser.parse_args()

    # obtain check_rule, build_rule and release
    check_rule = RULE.update if args.update else None

    release = None
    if args.release:
        build_rule = RULE.release
        release = args.release
    elif args.buildall or (args.cleanall and args.build):
        build_rule = RULE.buildall
    elif args.build:
        build_rule = RULE.build
    elif args.cleanall:
        build_rule = RULE.cleanall
    else:
        build_rule = None

    # extract repo and dpath
    if args.checkout:
        repo, dpath = urlarg_parse(args.checkout)
    else:
        repo, dpath = (None, '.')

    if args.path:
        dpath = args.path

    # check for 'hard' incompatibilities
    if args.verify and not args.release:
        incompatible_and_exit([], msg='--verify is only valid with --release')

    # proceed with actual processing
    try:
        # change CWD to parent directory to have cleaner relative paths in logs
        absdpath = os.path.abspath(dpath)
        os.chdir(os.path.abspath(os.path.join(dpath, '..')))
        dpath = os.path.basename(absdpath)

        rcomp = BComponent(ROOTCOMP, dpath, url=repo)

        rcomp.check_revision(check_rule)
        rcomp.check_subcomponents(check_rule)
        rcomp.check_targets(check_rule)
        rcomp.process(build_rule, vernum=release)
    except Exception as ex:
        print("\nERROR:", ex)
        print()
        sys.exit(-1)

    sys.exit(0)
