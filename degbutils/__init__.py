# -*- coding: utf-8 -*-
from . import _version

__version__ = _version.VERSION
__author__ = _version.AUTHOR
__author_email__ = _version.AUTHOR_EMAIL
__description__ = _version.DESCRIPTION
__url__ = _version.URL
