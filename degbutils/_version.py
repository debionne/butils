# -*- coding: utf-8 -*-
VERSION = '2.0.0'
AUTHOR = 'James Leonard'
AUTHOR_EMAIL = 'james.leonard@esrf.fr'
DESCRIPTION = "Build utilities for the ESRF Detector and Electronics group"
URL = "https://deg-svn.esrf.fr/svn/degbutils/"
