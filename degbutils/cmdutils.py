# -*- coding: utf-8 -*-
from __future__ import print_function
import subprocess
import sys
import os
import errno

def call(cmd_list, **kwargs):
    """
    Run command with arguments and return its output as string.
    The arguments are the same as for the Popen constructor
    
    If the exit code was non-zero it raises a CalledProcessError.  The
    CalledProcessError object will have the return code in the return code
    attribute and output in the output attribute. 
    """
    if not 'stdout' in kwargs:
        kwargs['stdout'] = subprocess.PIPE

    if 'env' in kwargs:
        kwargs['shell'] = True

    return check_output(cmd_list, **kwargs)



def check_output(*popenargs, **kwargs):
    """
    Run command with arguments and return its output as a byte string.

    If the exit code was non-zero it raises a CalledProcessError.  The
    CalledProcessError object will have the return code in the return code
    attribute and output in the output attribute.

    The arguments are the same as for the Popen constructor.  Example:

    >>> check_output(["ls", "-l", "/dev/null"])
    'crw-rw-rw- 1 root root 1, 3 Oct 18  2007 /dev/null\n'

    The stdout argument is not allowed as it is used internally.
    To capture standard error in the result, use stderr=STDOUT.

    >>> check_output(["/bin/sh", "-c",
    ...               "ls -l non_existent_file ; exit 0"],
    ...              stderr=STDOUT)
    'ls: non_existent_file: No such file or directory\n'
    """
    process = subprocess.Popen(*popenargs, **kwargs)
    output, unused_err = process.communicate()
    retcode = process.poll()

    if retcode:
        cmd = ' '.join(popenargs[0])
        raise subprocess.CalledProcessError(cmd=cmd, returncode=retcode, output=output)

    return output


def query_yes_no(question, default="yes"):
    """
    Ask a yes/no question via raw_input() and return the answer.

    :param str question: question that is presented to the user
    :param str default: presumed answer if the user just hits <Enter>.
                        It must be "yes" (the default), "no" or None (meaning
                        an answer is required of the user).

    :returns: True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def create_dirs(path):
    """
    Creates all sub-directories in the given path, raises an exception if one of
    the direcotires already exist.

    :param str path: path to create
    :raises OSError: if one of the subdirectories alreday exists
    :returns: None
    """
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
