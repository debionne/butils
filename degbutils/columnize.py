# -*- coding: utf-8 -*-
def columnize(sequence):
    cols = zip(*sequence)
    col_width = []
    s = ""
    
    for col in cols:
        word_len = max([len(word) for word in col]) + 4
        col_width.append(word_len)

    for row in sequence:
        for i in range(0, len(row)):
            word = row[i]
            s += word.ljust(col_width[i])

        s += '\n'

    return s
