
[root]
targets = <target1>, <target2>, ... <targetN>
    // the targets can be local components, external or subcomponents of external
    // a subcomponent target is described by:  "<externalA>.<subcompB>"
    / the notation above can be extended to multiple levels  

<<checking>>

- all the targets must refer to existing components (or subcomponents)



[local <name>]
path = <path>

<<checking>>

- if <path> does not exist or is not dir --> error
- obtain <rcs_url>, <rcs_ver>, if fails --> error
- if <rcs_url> relative to <rcs_url_parent> != <path> --> error
- if <rcs_rev> != <rcs_rev_parent> --> WARNING

<<update>>
   do nothing (should have been done by ancestors)
<<build>>
   same processing than externals



[external <name>]
path = <path>       // relative local path of the component
repo = <repo>       // repo root  ( https://deg-svn.esrf.fr/svn/<repo> )
rfolder = <rfolder> // full path in svn trunk  (/trunk/...)
verinfo = [<branch>,] {rev:[<rev> | HEAD | BASE | <version>}
                    // <branch> is full path:  (/branches/....  or /tags/...)
                    // <rev>, HEAD and BASE are the svn revision identifier
                    // <version> is a tag in /tags

<<checking>>

- if not <rfolder>, set <rfolder> = '/trunk'
- if <version> :
    - if not <branch> set <branch> = '/tags'
    - compose <url> in a way still to be defined
  else
    - if not <branch> set <branch> = <rfolder>
    - if no <rev>, set <rev> = BASE
    - compose <url> as <repo><branch>
- check that <url> exists in the server
- check consistency with <rfolder> (<branch> comes from <rfolder> in svn trunk)
- obtain parent of <path> and verify that exists and is local
- if <path> exists and is a dir
    - if it is not empty
        - obtain <rcs_url>, <rcs_ver>, if fails --> error
        - if <rcs_url> != <url> --> error
        - if <rcs_rev> != <rev> --> WARNING
      else (is empty) and in parent repo
        - delete and remove from parent
        - add to ignore list in parent
  else
    - checkout <url> and <rev>
