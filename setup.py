#!/usr/bin/env python
import degbutils
from distutils.core import setup

setup(name='degbutils',
    version=degbutils.__version__,
    description=degbutils.__description__,
    author=degbutils.__author__,
    author_email=degbutils.__author_email__,
    url=degbutils.__url__,
    packages=['degbutils'],
    entry_points={
        'console_scripts': [
            'butils = degbutils.main:main',
        ],
    },
)
