D&E Build Utility (butils)
==========================
A software project many times consist of several different components which 
need to be built and assembled correctly. The aim of D&E Build Utility is
to aid in this process, addressing the specific needs of the instrument
development projects driven by the Detector & Electronics Group. A special
file called build.spec is used to keep track of the different subcomponents (dependencies).
The tool can checkout a complete source tree, perform a build and create releases.

Usage
=====

The build utility (butils) looks for a file called build.spec in the root of 
the source tree. This file defines the targets and their dependencies. The
sources can be retrieved and built by the utility by using this file. A shell
script called finalize.sh is called upon completion of the build process. The
script is executed in an environment containing all the arguments for each of
the targets. The finalize.sh script can for instance be used to customize the 
assembly of the final instrument firmware. The utility is already installed
on RNICE and can be directly executed from there as described in the next
paragraph.
However in case butils has to be run from a computer where it is not installed,
the easiest way to do it is to execute:

::

  curl -ks https://deg-svn.esrf.fr/dist/butils/butils.py > butils.py
  python butils.py

::

When working in RNICE, the normal case, the only thing one needs
to do to get access to butils is to activate the D&E specific environment.

::

  source /segfs/deg/bin/deg_profile
  butils --help

::


  Usage: butils -c <url|name> <dest dir> 
         butils -c <url|name> 
         butils -b <source dir> <build args>
         butils -v <source dir> 
         butils -r <path> <version> 
         butils -cb <url|name> <dest dir> 


  Options:
    --version      show program's version number and exit
    -h, --help     show this help message and exit
    -b, --build    Build and verify release state of <path>
    -v, --verify   Verify if <path> is in a releasable state
    -c, --co       Checkout <name> <dest dir>
    -r, --release  Release <path> <version>

  where name is either a valid repository name or path on deg-svn. 
  The build args are passed as string on the format: 
  VAR_1=VALUE_1 VAR_2=VALUE_2 ...


Projects and Sub-projects
-------------------------
A project is specified by a build.spec file, this file defines the targets 
and their dependencies. A project can have any number of sub-projects, a 
sub-project is simply any folder under the project root containing a build.spec
file. A project can be referred to by its repository name i.e pepu, libdance, 
icepap and so on. The list of repositories can be found here:

::

    https://deg-svn.esrf.fr/svn/


Checking out a project is done by changing the working directory to the
desired location and then invoking butils with the repository name along
with the -c switch:

::

    butils -c pepu


A sub-project or a release can be accessed by adding /dev or /releases after
repository name and then appending the sub-project path or version number, i.e:

::

   # Firmware sub-project of pepu
   butils -c pepu/dev/firmware

   # Checkout version 3.5.0 of pepu
   butils -c pepu/releases/3.5.0

Building
--------
A build is launched by using the -b switch, the build process builds all
dependencies, recursively, and finally finishes with the targets. Sub-projects 
are built first (bottom-up) and the root project is built last. The targets 
in each build.spec file are built in the order they are defined. Targets defined
in a parent project takes priority over those defined in sub-projects. A warning
is issued in case of target re-definition and the target in the sub-project is 
ignored.

The dependencies are built in the order they are listed in the 'deps' instruction.

The shell script called finalize.sh is called at the end of the build if it 
exist, if not nothing is done. This script is optional and can be used to add 
custom logic to the end of the build process.

A release verification procedure is done at the end of the build process, this
procedure checks if the source tree is in a releasable state. The source is
considered to be releasable if there are:

 - No local uncommitted changes in the working-copy

 - The versions or revisions specified in the build.spec file corresponds 
   with whats on the file system.

A file called bof.txt (build output file) is created at the end of the build
process, this file contains information about what was built.

Build arguments can be passed to the buildmethods directly from the command
line by adding them after the project name. The build argument string should
have the following format VAR_1=VALUE_1 VAR_2=VALUE_2

::

   # Checkout and build development branch of pepu
   butils -cb pepu

   # Checkout and build firmware sub-project of pepu
   butils -cb pepu/dev/firmware

   # Checkout and build version 3.5.0 of pepu
   butils -cb pepu/releases/3.5.0

   # Checkout and build development branch of pepu, passing build arguments
   # DANCE_SDK_PLATFOR and RULE.
   butils -cb pepu DANCE_SDK_PLATFORM=seco_imx6 RULE=devel

Releasing:
----------
A release can only be made on an entire project to ensure that the release is 
consitent. Trying to create a relese on a subproject or fragment will give an
error message. A project can be released when the following conditions are 
fulfilled:

 - No local uncommitted changes in the working-copy

 - The versions or revisions specified in the build.spec file corresponds 
   with whats on the file system.

A project is released with the -r switch

::

   # Create release 1.0.0 of pepu
   butils -r pepu 1.0.0

The command above creates a release branch called 1.0.0, uploads the 
updated bof.txt file and a composite version of the build.spec file 
(with a snapshot of the version and revision numbers used at build).


More usage examples
-------------------

::

   curl -ks https://deg-svn.esrf.fr/dist/butils/butils.py > butils.py

   # Checkout and build the PEPU source tree
   python butils.py -cb pepu

   # Verify if pepu is in a releasable state
   python butils.py -v pepu

   # Create version x of PEPU
   python butils.py -r pepu x.0.0

The build.spec format
=====================

The build.spec files is used to define a set of targets and it's dependencies
for a project (set of sources). The build.spec contains information needed to
retrieve and build a project. This information can for instance be the version
or revision of dependencies, location of sources, build methods and arguments
to use.

A build specification file has optionally one buildargs section, optionally one
or several repo sections, optionally one or several dep sections and one or
several target sections.

A line can be commented by adding a hash sign (#) at the beginning of the line.


Build arguments (buildargs)
---------------------------

The buildargs section contains the arguments that should be passed to the build
process, for each target. The argument is passed as they are to the build method
that in turn are responsible for building the target.
::

    [buildargs]
    ARG_NAME_1 = VALUE_1
    ARG_NAME_N = VALUE_N

The arguments defined in buildargs can be overridden by passing arguments on
the command line. A target can in turn have individual 'specific' build
arguments which can't be overridden either through the command line or by
buildargs.

Repository (repo)
-----------------
It is possible to use any svn repository by defining a repository section.
The repository at https://deg-svn.esrf.fr is already predefined and is used
as a default if nothing else is specified. Valid keywords are: *url*, 
*tag_pattern*, *type*, *branch*.

**url:**
  The base url of the repository
 
**tag_pattern:**
  A pattern that describes how tags are made, i.e releases/{name}-{version}.

**type:**
  The repository type, valid values are: svn

**branch:**
  Default branch, i.e dev/.

The repo section for D&E svn would look like this:

::

  [repo deg-svn]
  url = https://deg-svn.esrf.fr/svn/
  tag_pattern = releases/{name}-{version}
  type = svn
  branch = dev/


Dependency (dep)
----------------
The dep section defines a dependency and the valid keywords are:
*version*, *repo*, and *path*.

**repo:**
  The repository to retrieve the source from, has to have the value of a 
  repo section. (Optional, deg-svn used if nothing else is given)

**version:**
  The version or revision to use, can also be used to specify branch to to use.
  Valid values are; <branch>, rev:<num> where branch is a valid branch i.e:
  branches/dev-new-feature/ and <num> is valid revision i.e 682. <branch> can
  be omitted which means that revision <num> on dev/will be used. A valid 
  version number i.e. 1.0.0 (the sources will be retrieved from releases/1.0.0/).
  The symbolic name last is used to denote the latest revision i.e rev:last.

**path**
  A relative path from the repository root of the depdency that are to be 
  considerd as the root of the depdency.

::

  # Last revision of dep name, use sub-path python/client as root
  [dep <name>]
  path = python/client/
  version = rev:last

  # Last revision on dev-new-feature branch
  [dep <name>]
  version = /branches/dev-new-feature, rev:last 


Target
------

The target section defines a target the valid keywords in a target section are:
*repo*, *path*, *method*, *args* and *deps*.

**path:**
  Sub-path that are considered the root of the target

**method:**
  The method to use when building, valid values are: cmake, vivado, and 
  - (minus sign). The minus sign - means no build method is used for this 
  target.

**args:**
  Specific arguments to pass to the build method, these arguments are
  final' and cannot be overridden by either buildargs or command line.
  They have to be either commented-out or removed in order not to be
  used. The format is: args=arg_name_1:value_1, arg_name_2:value_2

**deps:**
  Comma separated list of the dependencies related to the target. Valid
  values are; any dep names defined in the file i.e: deps = dep-1, dep-2
  , the minus sign (-) deps = - meaning no dependencies and nothing
  after the equals sign deps = meaning all dependencies. The dependencies are
  built in the order given in the list.

**depsdir**
  A path to use for dependencies the default is "components/". Only needed
  in more complex configurations.

::

    [target <name>]
    path = device_typeA/
    args = platform:specific_platform
    method = cmake
    deps = -

    [target <name>]
    method = cmake
    deps = nameA, nameB, nameC

Example build.spec
==================
An example of a full build.spec (for the PEPU project). Defining two targets
named *controller* and *hdl*.

::

  [buildargs]
  DANCE_SDK_PLATFORM = seco_imx6
  RULE = devel

  [target controller]
  method = cmake
  path = firmware/controller/

  [target hdl]
  path = firmware/hdl
  method = vivado
  deps = -

  [dep libdance]
  version = rev:last
