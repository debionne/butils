#!/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys
import base64
import tempfile
import zipfile
import shutil

from optparse import OptionParser

import subprocess

def call(cmd_line_list):
    try:
        return subprocess.check_output(cmd_line_list)
    except subprocess.CalledProcessError as ex:
        return str(ex.returncode)

TEMPLATE =\
"""#!/usr/bin/python
# -*- coding: utf-8 -*-

import base64
import os
import sys
import zipfile

ZIPDATA=\"\"\"\n{zip_data}\n\"\"\"

def unzip(path):
    zfile = zipfile.ZipFile(path)
    
    for name in zfile.namelist():
        (dirname, filename) = os.path.split(name)
        
        if not os.path.exists(dirname):
            os.mkdir(dirname)

        with open(name, 'w+') as fd:
            fd.write(zfile.read(name))

    zfile.close()

def install():
    zfile_path = os.path.join(os.getcwd(), 'degbutils.zip')
    
    with open(zfile_path, "wb") as f:
        f.write(base64.b64decode(ZIPDATA))

    unzip(zfile_path)
    os.remove(zfile_path)

if __name__ == '__main__':
    mfile_path = os.path.join(os.getcwd(), 'degbutils')
    
    #if not os.path.exists(mfile_path):
    install()

    from degbutils import main
    sys.path.append(os.path.abspath('.'))
    main.main()
""" 

def generate_zip(source_path):    
    all_files = []
    source_path = os.path.abspath(source_path)
    module_name = os.path.split(source_path)[1]
    
    for root, dirs, files in os.walk(source_path):
        for _file in files:
            if not '.svn' in root:
                name, ext = os.path.splitext(_file)
                if ext in [".py"] or name in ["butils"]:
                    all_files.append(_file)

    tmpdir = tempfile.mkdtemp()

    try:
        zf = os.path.join(tmpdir, module_name + ".zip")

        with zipfile.ZipFile(zf, "w", compression=zipfile.ZIP_DEFLATED) as z:
            for filename in all_files:
                print("Adding: %s" % filename)
                zf_filename = module_name + '/' + filename
                z.write(os.path.join(source_path, filename), zf_filename)

        with open(zf, "rb") as fp:
            data = fp.read()
            data = base64.b64encode(data).encode('utf8')
    finally:
        shutil.rmtree(tmpdir, ignore_errors=True)

    lines = []
    for i in range(0, len(data), 79):
        lines.append(data[i:i + 79])

    with open('butils.py', "w+") as df:
        formated_data = "\n".join(lines)
        formated_data = TEMPLATE.format(zip_data=formated_data)
        df.write(formated_data)

    print("Installer written to: butils.py")
    
def main():
    usage = '%prog <py-module> \n Takes the path to a python module and ' +\
            'writes the data (base64 encoded) to stdout' 
    parser = OptionParser(usage=usage, version='')
    options, args = parser.parse_args()
    
    if len(args) == 1:
        generate_zip(args[0])
    else:
        parser.print_help()
        sys.exit(2)

if __name__ == '__main__':
    main()
