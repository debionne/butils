import sys
import os

parent_dir = os.path.abspath(os.pardir)
sys.path.append(parent_dir)

from degbutils.rcsclient import BuildSpecRCSHandler

if __name__ == '__main__':
    repo_url = 'https://deg-svn.esrf.fr/svn/pydancelib/branches/dev-mo/'
    target_path = 'pydancelib_testco/'
    dep_root_path = os.path.join(target_path, 'external/')

    BuildSpecRCSHandler.project_checkout(repo_url, target_path)


