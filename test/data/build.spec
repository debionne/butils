[repo deg-svn]
type = svn
url = https://deg-svn.esrf.fr/svn/
tag_pattern = releases/{name}-{version}
branch = dev/

[dep libdeep]
repo = deg-svn
branch = branches/dev-mo/
path = python/libdeep
version = r61



